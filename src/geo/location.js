"use strict";
var Location = (function () {
    function Location(x, y, z, world, yaw, pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.world = world;
        this.yaw = yaw;
        this.pitch = pitch;
    }
    return Location;
}());
module.exports = Location;
