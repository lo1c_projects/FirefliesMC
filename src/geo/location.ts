class Location {

    constructor(public x: number, public y: number, public z: number, public world?: World, public yaw?: number, public pitch?: number) {

    }

}

export = Location;