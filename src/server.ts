class Server {

    whitelist = [];
    ops = [];

    constructor(config: any) {
        this.whitelist = config.whitelist;
        this.ops = config.ops;
    }

}