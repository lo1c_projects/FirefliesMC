import EntityBase = require("../entityBase");
import Location = require("../../geo/location");

class LivingEntity extends EntityBase {

    constructor(location: Location, name: string, displayName: string) {
        super(location);
    }

}

export = LivingEntity;