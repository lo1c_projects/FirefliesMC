"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var EntityBase = require("../entityBase");
var LivingEntity = (function (_super) {
    __extends(LivingEntity, _super);
    function LivingEntity(location, name, displayName) {
        return _super.call(this, location) || this;
    }
    return LivingEntity;
}(EntityBase));
module.exports = LivingEntity;
