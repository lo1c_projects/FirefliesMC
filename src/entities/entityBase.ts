import Location = require("../geo/location");

declare function require(name: string);
const crypto = require('crypto');

class EntityBase {

    static generate() {
        return crypto.randomBytes(32).toString('hex').replace(/(.{8})/g,"$1-").slice(0, -1);
    }

    static entities = {};

    eid: string;

    constructor(public location: Location) {
        this.eid = EntityBase.generate();
        EntityBase.entities[this.eid] = this;
    }

}

export = EntityBase;