"use strict";
var crypto = require('crypto');
var EntityBase = (function () {
    function EntityBase(location) {
        this.location = location;
        this.eid = EntityBase.generate();
        EntityBase.entities[this.eid] = this;
    }
    EntityBase.generate = function () {
        return crypto.randomBytes(32).toString('hex').replace(/(.{8})/g, "$1-").slice(0, -1);
    };
    EntityBase.entities = {};
    return EntityBase;
}());
module.exports = EntityBase;
